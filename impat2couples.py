#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  2 11:33:06 2024

@author: abettiol
"""

import argparse
from pathlib import Path
import itertools
import os, re
import numpy as np

from tools.ori import Ori
from pathlib import Path


def write_couple(fd, couple, fmt_output):
    
    formats = {
        "micmac":f"\t<Cple>{couple[0]} {couple[1]}</Cple>\n",
        "colmap":f"{couple[0]} {couple[1]}\n"}
    
    fd.write(formats[fmt_output])

def create_couples(dir_:Path,
                   patterns:list,
                   nadj:int=20,
                   intra:bool=True,
                   inter:bool=False,
                   ori_gnss:Path=Path("/Ori-GNSS"),
                   gnss_pattern:str=".*",
                   pos_shift:int=50,
                   fmt_output:str="micmac",
                   concat_output:bool=False,
                   duplicate:bool=False
                   ):
            
    try:
        
        print("\n--->reading ori_gnss", end="")
        ori_gnss = Ori(ori_gnss, gnss_pattern)
        print("<---")
        
    except ValueError:
        
        ori_gnss = None

    extension = {
        "micmac":"xml",
        "colmap":"txt"}

    types = {
        "Intra":intra,
        "Inter":inter,
        "GNSS":isinstance(ori_gnss, Ori)}
            
    output_types = '-'.join([key for key in types.keys() if types[key]])
    output_files = []
    
    if concat_output:
        
        concat_couples = set()
    
    axes = {pattern : sorted([img for img in os.listdir(dir_) if re.search(pattern, img)])
              
              for pattern in patterns}
    
    no_pose = set()
    
#---build intra/inter dict (im:[adj_im1, adj_im2...])
    
    for pattern, images in axes.items():
        
        print(f"-->computing {pattern} couples")
        print(f"\t {pattern} --> {len(images)} images.")

        couples = {}
            
        for index, image in enumerate(images):

            couples[image] = []
            
            if intra:
            
                adj_intra_idx = filter(lambda x : x in range(len(images)),
                             
                                 [index + n for n in range(-nadj, nadj + 1) if n!=0])
                                
                for idx in adj_intra_idx:
                    
                    couples[image].append(images[idx])
                
            if inter:
                
                for inter_pattern , inter_images in filter(lambda x : x[0] != pattern,
                                                           axes.items()):
                    
                    
                    #--no Ori-GNSS considering that idx of each axes are equal
                    if not isinstance(ori_gnss, Ori):
                        
                        adj_inter_idx = filter(lambda x : x in range(len(inter_images)),
                                     
                                         [index + n for n in range(-nadj, nadj + 1)])
                        
                    #--Ori-GNSS to associate adjacent images around closest image
                    else:
                        
                        #--noms des images candidates pour être la plus proche 
                        
                       
                        potential_img = [inter_images[idx] for idx in filter(lambda x : x in range(len(inter_images)),
                                     
                                          [index + n for n in range(-pos_shift, pos_shift + 1)])]
                                                
                        #--si l'image d'origine n'est pas orientée, couple classique
                        if np.isnan(ori_gnss.get_centre(image)).any():
                            
                            print(f"L'image {image} n'a pas de position.")
                            
                            no_pose.add(image)
                            
                            adj_inter_idx = filter(lambda x : x in range(len(inter_images)),
                                         
                                             [index + n for n in range(-nadj, nadj + 1)])
                            
                        else:
                            
                            #--recherche de l'image la plus proche
                            diff = ori_gnss.get_multi_centre(potential_img) - ori_gnss.get_centre(image)
                            
                            if not len(diff):
                                
                                print("Abandon, le décalage est trop important, renseigner un pos_shift plus grand.")
                            
                                return

                            closest_idx = np.nanargmin(np.sqrt(diff[:,0]**2 + diff[:,1]**2))
                            closest_img = potential_img[closest_idx]
                            
                            print("\t close2 : ", image, "--->", closest_img, f"dplani = {np.sqrt(diff[closest_idx,0]**2 + diff[closest_idx,1]**2)}")
                            
                            adj_inter_idx = filter(lambda x : x in range(len(inter_images)),
                                         
                                             [inter_images.index(closest_img) + n for n in range(-nadj, nadj + 1)])
                            
                    for idx in adj_inter_idx:
                        
                        couples[image].append(inter_images[idx])
        print("\t<---")

                    
        #---write couples in one file for each axe
        print(f"--->saving {pattern} couples")
        
        with open(dir_.joinpath(f"{pattern.split('.*')[0]}-{output_types}-Couples.{extension[fmt_output]}"),
                      "w", encoding="utf-8") as fd:
            
            if fmt_output == "micmac":
            
                fd.write('<?xml version="1.0" ?>\n')
                fd.write("<SauvegardeNamedRel>\n")
                
            saved_couples = set()
                
            for im, adj in couples.items():
                
                for adj_im in adj:
                    
                    if not duplicate:
    
                        if not (im, adj_im) in saved_couples and not (adj_im, im) in saved_couples:
                            
                            write_couple(fd, (im, adj_im), fmt_output)
                    
                    else:
                        
                        write_couple(fd, (im, adj_im), fmt_output)
    
                    saved_couples.add((im, adj_im))
            
            if fmt_output == "micmac":
                
                fd.write("</SauvegardeNamedRel>\n")
            
            if concat_output:
                
                output_files.append(f"{pattern.split('.*')[0]}-{output_types}-Couples.{extension[fmt_output]}")
                concat_couples.update(saved_couples)                
                
                
        print(f"\t{pattern} --> {pattern.split('.*')[0]}-{output_types}-Couples.{extension[fmt_output]} --> {len(saved_couples)} couples", end="")
        print("<---")

        
    #---write global file if concat
    lines = 0
    
    if concat_output:
        
        print("--->saving concat output")
        saved_couples = set()
        
        with open(dir_.joinpath(f"{output_types}-Couples.{extension[fmt_output]}"),
                      "w", encoding="utf-8") as fd:
            
            if fmt_output == "micmac":
                
                fd.write('<?xml version="1.0" ?>\n')
                fd.write("<SauvegardeNamedRel>\n")
            
            for couple in concat_couples:
                
                if not duplicate:
                    
                    if not couple in saved_couples and not couple[::-1] in saved_couples:
                    
                        write_couple(fd, couple, fmt_output)
                        lines+=1
                        
                else:
                         
                    write_couple(fd, couple, fmt_output)
                    lines+=1

                
                saved_couples.add(couple)

                
            if fmt_output == "micmac":
                
                fd.write("</SauvegardeNamedRel>\n")
            
            print(f"\t{output_types}-Couples.{extension[fmt_output]} --> {lines} couples", end="")
            print("<---")
    
    
    if isinstance(ori_gnss, Ori):

        print(f"{len(no_pose)} images n'ont pas de position ({len(no_pose)/sum([len(images) for images in axes.values()])*100} %).")



if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="*************** create_couples ***************")
    
    parser.add_argument("dir", type=str, help="images directory (all images should be in the same directory)")
    parser.add_argument("patterns", nargs='+', help="list of image patterns (one for each axe) : only file name assuming all images are in the same directory")
    
    parser.add_argument("--nadj", type=int, help="size of the window to look for neighbour image (Default=20)",
                        default=20)
    
    parser.add_argument("--intra", type=str, help="compute couples for each intra-axe {True,False} (Default=True)",
                        default="True")
    
    parser.add_argument("--inter", type=str, help="compute couples for each inter-axe  {True,False} (Default=False)",
                        default="False")
    
    parser.add_argument("-gnss", "--ori_gnss", type=str, help="path to gnss poses of images which will be taking to account when computing inter-axes couples\
                        (Default='')", default="/Ori-GNSS")
                        
    parser.add_argument("-gnss_pat", "--gnss_pattern", type=str, help="pattern to filter gnss poses when ori_gnss is an input (Default='.*')", default=".*")
    
    parser.add_argument("-ps", "--pos_shift", type=str, help="when computing inter-axes couples with ori_gnss poses pos_shift is the half size of the window\
                        in which the program is looking for closest image pose (Default=50)", default=50)
                        
    parser.add_argument("-ofmt", "--fmt_output", type=str, help="format of output file {'micmac', 'colmap'} Default='micmac')",
                        default="micmac") 
    
    parser.add_argument("-oconcat", "--concat_output", type=str, help="concatenate output files in one file {True, False} (Default=False)",
                        default="False") 
    
    parser.add_argument("-dpc", "--duplicate", type=str, help="duplicate couples (intra AND inter if choosen) {True, False} (Default=False}",
                        default="False") 
    
    
    args = parser.parse_args()
    
    print("\n*************** create_couples ***************")
    
    print("IMDIR         :", args.dir)
    print("PATTERNS      :", args.patterns)
    print("N ADJ         :", args.nadj)
    print("INTRA         :", args.intra)
    print("INTER         :", args.inter)
    print("GNSS          :", args.ori_gnss)
    print("GNSS PATTERN  :", args.gnss_pattern)
    print("POS SHIFT     :", args.pos_shift)
    print("FORMAT OUTPUT :", args.fmt_output)
    print("CONCAT OUTPUT :", args.concat_output)
    print("DUPLICATE     :", args.duplicate)

    
    evaluate = {
        "True":True,
        "true":True,
        "False":False,
        "false":False
        }
    
    if not Path(args.dir).is_dir():
        
        print(f"!! {args.dir} n'est pas un dossier.")
        
    
    elif args.ori_gnss != "/Ori-GNSS" and not Path(args.ori_gnss).is_dir():
        
        print(f"!! {args.ori_gnss} n'est pas un dossier.")
        
    elif args.fmt_output not in ('micmac', 'colmap'):
        
        print(f"!! {args.fmt_output} n'est pas un format de sortie.")
    
    else:
        
        create_couples(dir_          = Path(args.dir),
                       patterns      = args.patterns,
                       nadj          = int(args.nadj),
                       intra         = evaluate[args.intra],
                       inter         = evaluate[args.inter],
                       ori_gnss      = Path(args.ori_gnss),
                       gnss_pattern  = args.gnss_pattern,
                       pos_shift     = int(args.pos_shift),
                       fmt_output    = args.fmt_output,
                       concat_output = evaluate[args.concat_output],
                       duplicate     = evaluate[args.duplicate]
                        )    
    
    print("***********************************************\n")
