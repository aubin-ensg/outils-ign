#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 24 10:06:36 2024

@author: abettiol
"""

from pathlib import Path
import re
from xml.etree import ElementTree as ET
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import colormaps as cmap
from tools.angles_tools import euler2rotmat, rotmat2euler
from tools.plot_tools import plot_stats


plt.rcParams['text.usetex'] = True
plt.rcParams['figure.facecolor'] = "#FAFAFA"


class Ori:

    def __init__(self,
                 ori_path:Path,
                 pattern:str=".*"
                 ):

        if not ori_path.is_dir():
            
            raise ValueError(f"{ori_path} n'est pas un dossier.")

        self.path          = ori_path
        self.pattern      = pattern
        
        ori_types = {
            
            self.path.joinpath("Residus.xml"):"tapas",
            self.path.joinpath("Result-GCP-Bascule.xml"):"gcp_basc",
            self.path.joinpath("Result-Center-Bascule.xml"):"center_basc",
            
            
            }
        
        try :
            
            self.res_file, self.ori_type = list(filter(lambda x : x[0].is_file(), ori_types.items()))[0]
    
        except IndexError:
            
            
            self.res_file, self.ori_type = None, "convert"
        
        if self.ori_type == "tapas":
            
            self.res = self.read_residus()        
        
        elif self.ori_type == ["gcp_basc", "center_basc"]:
            
            self.res = self.read_result_gcp()
            
        self.images       = self.find_images()
        self.poses        = self.find_poses()


    def __len__(self):
        
        return len(self.images)

    def find_images(self):

        images = []

        for ori_file in self.path.glob("Orientation-*.xml"):

            match = re.match(f"Orientation-({self.pattern}).xml", ori_file.name)
            
            if match:
                
                images.append(match.group(1))

        images.sort()

        return images

    def find_poses(self):

        poses = {}

        for im in self.images:

            with open(self.path.joinpath(f"Orientation-{im}.xml"), 'r', encoding="utf-8") as fs:

                tree = ET.parse(fs)
                root = tree.getroot()
                
                if self.ori_type == "convert":
                    
                    W, P, K = np.array(root.find("./Externe/ParamRotation/CodageAngulaire").text.split(" "), np.float64)[:]
                    
                    rotmat = euler2rotmat(P, W, K, indeg=True)
                    
                    centre = np.array([root.find("./Externe/Centre").text.split(' ')], np.float64)
                    
                    
                else:
                    
                    rotmat = np.array([root.find("./OrientationConique/Externe/ParamRotation/CodageMatr/L1").text.split(" "),
                                       root.find("./OrientationConique/Externe/ParamRotation/CodageMatr/L2").text.split(" "),
                                       root.find("./OrientationConique/Externe/ParamRotation/CodageMatr/L3").text.split(" ")], np.float64)

                    centre = np.array([root.find("./OrientationConique/Externe/Centre").text.split(' ')], np.float64)

            poses[im] = [centre, rotmat]

        return poses
        
    
    def read_residus(self):
        
        tree = ET.parse(self.res_file)
        root = tree.getroot()
        
        res = pd.DataFrame(columns=["N", "res", "percok", "nb_pts", "nb_pts_mul"])

        images  = root.findall("./Iters")[-1].findall("OneIm") # get OneIm elements from last iterations
        
        for index, oneim in enumerate(images):
            
            if re.match(self.pattern, oneim.find("./Name").text) :
                
                child_values       = [child.text for child in oneim]
                res.loc[index] = child_values
        
        res = res.set_index("N").astype({"res"      :"float32",
                                        "percok"    :"float32",
                                        "nb_pts"    :"int32",
                                        "nb_pts_mul":"int32"})
        
        return res
    
    
    def read_result_gcp(self):
        
        tree = ET.parse(self.res_file)
        root = tree.getroot()
        
        basc_gcp = {
            "basc":np.zeros([3,3]),
            "trans":np.zeros([3,1]),
            "scale":1}
        
        rotation = root.find("./Param/ParamRotation")
        
        basc_gcp["basc"] = np.array([[line.text.split(" ") for line in rotation[:-1]]], dtype=np.float64)
        
        basc_gcp["trans"] = np.array([[root.find("./Param/Trans").text.split(" ")]], dtype=np.float64)
        
        basc_gcp["scale"] = float(root.find("./Param/Scale").text)
        
        result = pd.DataFrame(columns=["GCP", "dX", "dY", "dZ"])
        
        res = root.findall("./Residus")
        
        for index, res in enumerate(res):
            
            offset = res.find("./Offset").text.split(" ")
            name   = res.find("./Name").text
            
            try : 
                
                name = int(name)
                
            except ValueError:
                pass
            
            result.loc[index] = [name] + offset
        
        result = result.set_index("GCP").sort_index().astype({"dX":"float32",
                                                 "dY":"float32",
                                                 "dZ":"float32"})
    
        return basc_gcp, result
    
    
    def plot_res(self, pattern, multipat=[], plot=True):
        
        if "res" not in self.__dict__.keys():
            
            print("Aucun fichier de résidus sur l'orientation relative trouvé dans cette instance : impossible d'afficher le graphe.")
        
            return None
        
        multipat.insert(0, pattern)
        
        plot_index = []
            
        fig, (ax0, ax1) = plt.subplots(nrows=2, ncols=1)
        
        res_scatter = []
        percok_scatter = []
            
        for index, pat in enumerate(multipat):
            
            x_res = [N for N in self.res.index if re.match(pat, N)]
            y_res = self.res.loc[x_res]
            
            res = ax0.scatter(np.arange(len(y_res)), y_res["res"], marker='+', color=cmap["Set2"](index))
            res_scatter.append(res)
            
            percok = ax1.scatter(np.arange(len(y_res)), y_res["percok"], marker='+', color=cmap["Set2"](index))
            percok_scatter.append(percok)
            
            plot_index += x_res
        
        res_legend = ax0.legend(res_scatter, [pat.split('.')[0] for pat in multipat], loc='upper left')
        percok_legend = ax1.legend(percok_scatter, [pat.split('.')[0] for pat in multipat], loc='upper left')
        
        ax0.add_artist(res_legend)
        ax1.add_artist(percok_legend)
        
        plot_stats(ax0, self.res["res"].loc[plot_index], multipat=multipat, loc='lower left', unit="px",
                   title="Résidus de l'ajustement de faisceaux sur les mesures images des points de liaison",
                   ylabel="residus", fmt=".1f", avrg_pref="")
        
        plot_stats(ax1, self.res["percok"].loc[plot_index], multipat=multipat, loc='lower left', unit="\\%",
                   title="Pourcentage de points de liaisons intégrés à l'ajustement de faisceaux",
                   ylabel="pourcentage", fmt=".1f", avrg_pref="")
        
        fig.suptitle("Indicateurs qualité de l'ajustement de faisceaux", fontsize="15")

        if plot:
            
            plt.show()

            return fig
        
        else:
            
            print(ax0.collections)
            
            return ax0.collections
        
    def plot_result_basc(self):
        
        if "res_basc_gcp" not in self.__dict__.keys():
            
            print("Aucun fichier de résidus sur la bascule trouvé dans cette instance : impossible d'afficher le graphe.")
        
            return None
        
        self.res_basc_gcp["distHz"] = np.sqrt(self.res_basc_gcp["dX"]**2 + self.res_basc_gcp["dY"]**2)
        self.res_basc_gcp["distV"]  = np.sqrt(self.res_basc_gcp["dZ"]**2)
        self.res_basc_gcp["dist3d"] = np.sqrt(self.res_basc_gcp["dX"]**2 + self.res_basc_gcp["dY"]**2 + self.res_basc_gcp["dZ"]**2)
        
        plots = [{"dist":"distHz",
                  "title":"Distance horizontale"},
                 
                 {"dist":"distV",
                  "title":"Distance verticale"},
                 
                 {"dist":"dist3d",
                  "title":"Distance 3d"}]
        
        fig, (ax0, ax1, ax2) = plt.subplots(nrows=1, ncols=3)

        for index, ax in enumerate(fig.axes):
            
            ax.scatter(np.arange(len(self.res_basc_gcp.index)), self.res_basc_gcp[plots[index]["dist"]], marker='+', color=cmap["Set2"](index))
            
            ax.set_xticks(np.arange(len(self.res_basc_gcp.index)), self.res_basc_gcp.index)
    
            plot_stats(ax, self.res_basc_gcp[plots[index]["dist"]], only="avrg_min_max", shift_label=.5,
                       avrg_pref="", title=plots[index]["title"], loc="best", ylabel="résidus", unit="m", xlabel="GCP n°")

        fig.suptitle("Indicateurs qualité de l'estimation de la bascule sur les points d'appui (GCP)", fontsize="15")

        plt.show()

        return fig
    
    def plot_la(self, subpatterns, multisubpatterns=[]):
        
        multisubpatterns.insert(0, subpatterns)
        
        fig, axs = plt.subplots(nrows=4, ncols=2)
        
        for subpat in multisubpatterns:
            
            subset1 = [im for im in self.images if re.match(subpat[0], im)]
            subset2 = [im for im in self.images if re.match(subpat[1], im)]
            
            centre_diff, euler_diff = np.zeros([len(subset1), 3]), np.zeros([len(subset1), 3])
            
            for index, im in enumerate(subset1):
                
                centre_diff[index,:] = self.get_centre(im) - self.get_centre(subset2[index])
                euler_diff[index,:]  = self.get_euler(im) - self.get_euler(subset2[index])
                
            norm3d = np.sqrt(centre_diff[:,0]**2 + centre_diff[:,1]**2 + centre_diff[:,2]**2)
            
            axs[0,0].scatter(np.arange(len(centre_diff)), centre_diff[:,0], marker="+")
            axs[1,0].scatter(np.arange(len(centre_diff)), centre_diff[:,1], marker="+")
            axs[2,0].scatter(np.arange(len(centre_diff)), centre_diff[:,2], marker="+")
            axs[3,0].scatter(np.arange(len(centre_diff)), norm3d, marker="+")
            
            
            axs[0,1].scatter(np.arange(len(euler_diff)), euler_diff[:,0], marker="+")
            axs[1,1].scatter(np.arange(len(euler_diff)), euler_diff[:,1], marker="+")
            axs[2,1].scatter(np.arange(len(euler_diff)), euler_diff[:,2], marker="+")

        return centre_diff

    def get_pose(self, im):

        try:
            return self.poses[im]
        
        except KeyError:
            return [np.ones([1,3])*np.nan,
                    np.ones([3,3])*np.nan]

    def get_rotmat(self, im):
        
        return self.get_pose(im)[1]
    
    def get_euler(self, im, outdeg=False):

        return np.array([rotmat2euler(self.get_rotmat(im), outdeg)])
        
    def get_centre(self, im):
        
        return self.get_pose(im)[0]
    
    def get_multi_centre(self, images:list):
        
        multi_centre = np.ones([len(images), 3])*np.nan

        for index, image in enumerate(images):
            
            multi_centre[index,:] = self.get_centre(image)
            
        return multi_centre
    
    def get_res(self, im):
        
        return self.res.loc[im]


    def set_centre(self, im, x, y, z):
        
        self.poses[im][0] = np.array([x,y,z]).T
        
    def save_ori(self, out_name, pat, overwrite=False):
        
        if overwrite:
            
            fd = open(self.path.parent.joinpath(out_name + ".txt"), 'w')
            
        else:
            
            fd = open(self.path.parent.joinpath(out_name + ".txt"), 'a')
        
        for key, value in self.poses.items():
            
            if re.match(pat, key):
                
                print(f"Exporting {key}...")
                centre = f"{float(value[0].T[0])} {float(value[0].T[1])} {float(value[0].T[2])}"
                euler = np.array([rotmat2euler(value[1], outdeg=True)])
                rot = f"{float(euler.T[0])} {float(euler.T[1])} {float(euler.T[2])}"
                
                fd.write(key + ' ' + centre + ' ' + rot +'\n')
        
        print(f"Orientation save as : {self.path.parent.joinpath(out_name + '.txt')}")
        
        fd.close()
