#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 12:48:56 2024

@author: abettiol
"""

import pandas as pd
import warnings
warnings.filterwarnings("error")

from pathlib import Path
from tools.transcoord import RGF93_llh_2_RGF93_ENh, RGF93_llh_2_RGF93_XYZ, alt_lin
from tools.angles_tools import quat2euler, quatnorm2quat
from numpy import pi


# -- Fonctions de lecture

EXTEVENTLOG_COLUMNS = ["wsec", "wk", "status", "lat", "lon", "h", "dHz", "dV",
                       "q2", "q1", "q3", "code1", "code2"]
FAIL = "\t!! -- Echec de la procédure."
SUCESS = "\t\t--> Succès de la procédure."
D2R = pi/180

def read_exteventlog(fpath:Path,
                     status:list=['R','F','N',''],
                     convert2_enh:bool=False,
                     convert2_xyz:bool=False,
                     convert2_uvw:bool=False,
                     log:bool=True
                     ):
    
    if not fpath.is_file():
        
        print(f"!! -- {fpath} n'est pas un fichier.")
        print(FAIL)
        
        return None
    
    if log :
        
        print(f">> -- Lecture de {fpath.name} -- ")
    
    try:
        dat = pd.read_csv(fpath, names=EXTEVENTLOG_COLUMNS, index_col=False)
                          
    except pd.errors.ParserWarning:
        
        print(f"\t! -- {fpath} n'est pas conforme au format implémenté"\
              + "dans ce programme. ")
        print(FAIL)
        
        return None
    
    if log :
        
        print(f"\t> - {fpath.name} : {len(dat)} events.")
    
    dat_filter = dat.loc[dat["status"].isin(status)]
    
    if log:
        
        print(f"\t> - nb. obs. {status} : {len(dat_filter)} events.")
        print(f"\t> - prec. obs. {status} : ")
        
        prec = [
            {"name":dist,
             "min":dat_filter[dist].min(),
             "max":dat_filter[dist].max(),
             "mean":dat_filter[dist].mean()
             } for dist in ["dV", "dHz"]]
       
        for dist in prec:
            
            print(f"\t\t\t|> min  : {dist['min']:.3f} m"\
                  + f"\n\t\t\t|> max  : {dist['max']:.3f} m"\
                  + f"\t({dist['name']})"\
                  + f"\n\t\t\t|> mean : {dist['mean']:.3f} m")
                
    if convert2_enh:
        
        dat_filter[["E_l93", "N_l93", "h"]] = \
            dat_filter.apply(lambda x : RGF93_llh_2_RGF93_ENh(D2R*x['lon'], 
                                                              D2R*x['lat'], 
                                                              x['h']),
                             axis=1, result_type="expand")
            
        print("\t> - Coordonnées converties llh_rgf93 --> ENh_l93 : "
              + "\n\t\t> alt. lin. ~ "\
              + f"{alt_lin(dat_filter['N_l93'].mean()):.1f} ppm")
        
    if convert2_xyz:
        
        dat_filter[["X", "Y", "Z"]] = \
            dat_filter.apply(lambda x : RGF93_llh_2_RGF93_XYZ(D2R*x['lon'], 
                                                              D2R*x['lat'],
                                                              x['h']),
                             axis=1, result_type="expand")
            
        print("\t> - Coordonnées converties llh_rgf93 --> xyz_rgf93.")
        
    
    if convert2_uvw:
        
        dat_filter["q0"] = \
            dat_filter.apply(lambda x : quatnorm2quat(x["q1"],
                                                      x["q2"],
                                                      x["q3"]),
                             axis=1)
        
        dat_filter[["U", "V", "W"]] = \
            dat_filter.apply(lambda x : quat2euler(x["q0"],
                                                   x["q1"],
                                                   x["q2"],
                                                   x["q3"],
                                                   outdeg=True)[:],
                            axis=1, result_type="expand")
        
        print("\t> - Orientations converties q1, q2, q3, q4"\
              + " --> UVW = Roll, Pitch, Yaw.")


    print(SUCESS)

    return dat_filter