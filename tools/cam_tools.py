#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 15:20:09 2024

@author: abettiol
"""

from pathlib import Path
import re, os
from numpy import pi
from PIL import Image
import gpsdatetime as gpst
import pandas as pd


# -- Méthodes de lecture

FAIL = "\t!! -- Echec de la procédure."
SUCESS = "\t--> Succès de la procédure."
D2R = pi/180

def read_imgdir(dpath:Path,
                pattern:str=".*JPG",
                convert2_wsec:bool=False
                ):
    
    if not dpath.is_dir():
        
        print(f"!! -- {dpath} n'est pas un dossier.")
        print(FAIL)
        
        return None
        
    print(f"\n>> -- Lecture de {dpath.name}/{pattern} -- ")
    
    regex = re.compile(pattern)
    
    imgs = [{"path":dpath.joinpath(img)}
             for img in os.listdir(dpath) if re.search(regex, img)]
    
    print(f"\t> - {dpath.name}/{pattern} : {len(imgs)} images.")
    
    if not len(imgs):
        
        print("\t! - {dpath.name}/{pattern} ne correspond à aucune image.")
        print("\t" + FAIL)
        
        return None
    
    if convert2_wsec:
        
        for index, im in enumerate(imgs):
            
            im["xif_wsec"] = xifdatetime(im["path"], wsec=True)[1]
            
        print("\t> - EXIF datetime convertis en wsec.")
        
    print(SUCESS + '\n')
 
    return pd.DataFrame.from_dict(imgs, orient="columns").sort_values\
        ("path",ignore_index=True)

# -- Méthodes d'exploitation des EXIF

def xifdatetime(impath:Path,
                wsec=False
                ):
    
    im = Image.open(impath)
    exif = im.getexif()
    im.close()
    
    date, time = exif[306].split(" ")
    dd, mm, yyyy = date.split(':')[::-1]
    hh, mn, ss    = time.split(':')
        
    if wsec:
        
        gpstime = gpst.gpsdatetime(yyyy=int(yyyy), 
                                   mon=int(mm),
                                   dd=int(dd),
                                   h=int(hh),
                                   min=int(mn),
                                   sec=float(ss) + 18
                                   )
        
        return gpstime.wk, gpstime.wsec
            
    else:
        
        return f"{dd}:{mm}:{yyyy} {hh}:{mn}:{ss}"