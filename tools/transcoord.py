#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 14:39:14 2024

@author: abettiol

"""
import math as m
import numpy as np

R2D = 180/np.pi
D2R = 1/R2D
L93 = {
       "k0":0.99905103,
       "Y0":6600000
       }

R = 6378000

def lat_iso(lat, e, inrad=True):
    
    if not inrad :
        
        lat = R2D * lat
    
    L = 1/2 * np.log( (1+np.sin(lat))/(1-np.sin(lat)) ) - e/2 *\
        np.log( (1 + e*np.sin(lat))/(1-e*np.sin(lat)) )
    
    return L

def alt_lin(Y, proj=L93):
    """

    Parameters
    ----------
    Y : float
        Coordonnée Nord du point.
    proj : string, optional
        Projection dans laquelle Y est donnée. The default is L93.

    Returns
    -------
    float
        Altération linéaire au point donné en ppm (valeur approchée).

    """
        
    d = Y - proj["Y0"]
    m = proj["k0"] * (1 + d**2/(2*R**2))
    
    return (m - 1) * 1e6

#degree to radian
d2r=m.pi/180

#radian to degree
r2d=1/d2r

#constantes de la projection Lambert-93
L93_Lon0=3*d2r
L93_Lat0=(46+30/60)*d2r
L93_Lat1=44*d2r
L93_Lat2=49*d2r
L93_E0=700000
L93_N0=6600000

#IAG GRS80
a_GRS80=6378137.000
e2_GRS80=0.006694380022900788

###############################################################################
###############################################################################
def lat_2_latiso(lat,e):
    """
    compute isometric latitude from geographic latitude

    Parameters
    ----------
    lat : float
        geographic latitude (rad).
    e2 : float
         square of the first eccentricity of the ellipsoid.

    Returns
    -------
    L : float
        isometric latitude.

    """

    num=1-e*m.sin(lat)
    dnum=1+e*m.sin(lat)
    L=m.log(m.tan((m.pi/4)+(lat/2)))+e*m.log(num/dnum)/2

    return L
###############################################################################
###############################################################################
def calc_params_pcc(a,e,lon0,lat0,lat1,lat2,E0,N0):
    """
    compute useful parameters of the "pcc": Projection Conique Conforme

    Parameters
    ----------
    a : float
        semi-major axis of the ellipsoid (m).
    e2 : float
         square of the first eccentricity of the ellipsoid.
    lon0 : float
        longitude at the origine with respect to the reference meridian (rad).
    lat0 : float
        latitude at the origine (rad).
    lat1 : float
        latitude of the 1st automecoic parallel (rad).
    lat2 : float
        latitude of the 2nd automecoic parallel (rad).
    E0 : int
        est component of the point at the origin (m).
    N0 : int
        north component of the point at the origin (m).

    Returns
    -------
    lonc : float
        longitude at the origine with respect to the reference meridian (rad).
    n : float
        exponent of the projection.
    C : float
        projection constant (m).
    Es : float
        east component of the pole in projection (m).
    Ns : float
        north component of the pole in projection (m).

    """

    #longitude origine par rapport au meridien origine
    lonc=lon0

    #exposant de la projection
    N1=a/(m.sqrt(1-e**2*m.sin(lat1)**2))
    N2=a/(m.sqrt(1-e**2*m.sin(lat2)**2))

    L1=lat_2_latiso(lat1,e)
    L2=lat_2_latiso(lat2,e)

    n=m.log((N2*m.cos(lat2))/(N1*m.cos(lat1)))/(L1-L2)

    #constante de la projection
    C=N1*m.cos(lat1)*m.exp(n*L1)/n

    #coordonees du pole en projection
    Es=E0

    if(lat0 == m.pi/2):
        Ns=N0
    else:
        L0=lat_2_latiso(lat0,e)

    Ns=N0+C*m.exp(-n*L0)

    return lonc,n,C,Es,Ns
###############################################################################
###############################################################################
def llh_2_ENh_ccl(lon,lat,h,a,e2,lon0,lat0,lat1,lat2,E0,N0):
    """
    convert geographic coordinates to planar coordinates with respect to
    "ccl" : Conique Conforme de Lambert

    Parameters
    ----------
    lon : float
        geographic longitude (rad).
    lat : float
        geographic latitude (rad).
    h : float
        ellipsoidal height (m).
    a : float
        semi-major axis of the ellipsoid (m).
    e2 : float
        square of the first eccentricity of the ellipsoid.
    lon0 : float
        longitude at the origine with respect to the reference meridian (rad).
    lat0 : float
        latitude at the origine (rad).
    lat1 : float
        latitude of the 1st automecoic parallel (rad).
    lat2 : float
        latitude of the 2nd automecoic parallel (rad).
    E0 : int
        east component of the point at the origin (m).
    N0 : int
        north component of the point at the origin (m).

    Returns
    -------
    E : float
        est component of the point (m).
    N : float
        north component of the point (m).
    h : float
        ellipsoidal height (m).

    """

    #calcul de la latitude isometrique sur l'ellipsoide
    e=m.sqrt(e2)
    L=lat_2_latiso(lat,e)

    #determination des parametres de calcul
    lonc,n,C,Es,Ns=calc_params_pcc(a,e,lon0,lat0,lat1,lat2,E0,N0)

    #calcul des coordonnees planes
    E=Es+C*m.exp(-n*L)*m.sin(n*(lon-lonc))
    N=Ns-C*m.exp(-n*L)*m.cos(n*(lon-lonc))

    return E,N,h
###############################################################################
###############################################################################
def N_2_ccCsts(N):
    """
    generate csts based on N value corresponding to zone number 
    of the french Conic Conformal 

    Parameters
    ----------
    N : int
        zone number.

    Returns
    -------
    CC_Lon0 : float
        longitude at the origine with respect to the reference meridian (rad)..
    CC_Lat0 : float
        latitude at the origine (rad).
    CC_Lat1 : float
        latitude of the 1st automecoic parallel (rad).
    CC_Lat2 : float
        latitude of the 2nd automecoic parallel (rad).
    CC_E0 : int
        est component of the point at the origin (m).
    CC_N0 : int
        north component of the point at the origin (m).

    """
    if(N>=1 and N<=9):
        CC_Lon0=3*d2r
        CC_Lat0=(41+N)*d2r
        CC_Lat1=CC_Lat0+0.75*d2r
        CC_Lat2=CC_Lat0-0.75*d2r
        CC_E0=1700000
        CC_N0=(N*1000000)+200000
    else:
        sys.exit("N value out of range")
    return CC_Lon0,CC_Lat0,CC_Lat1,CC_Lat2,CC_E0,CC_N0
###############################################################################
###############################################################################
def RGF93_llh_2_RGF93_ENh(lon,lat,h,ID=93):

    if(ID==93):
        Lon0=L93_Lon0
        Lat0=L93_Lat0
        Lat1=L93_Lat1
        Lat2=L93_Lat2
        E0=L93_E0
        N0=L93_N0
    else:
        Lon0,Lat0,Lat1,Lat2,E0,N0=N_2_ccCsts(ID)

    E,N,h=llh_2_ENh_ccl(lon,lat,h,a_GRS80,e2_GRS80,Lon0,Lat0,Lat1,Lat2,E0,N0)

    return E,N,h
###############################################################################
###############################################################################
def RGF93_llh_2_RGF93_XYZ(lon,lat,h):
    
    w = m.sqrt(1 - e2_GRS80 * m.sin(lat)**2)
    N = a_GRS80/w
    
    X = (N+h) * m.cos(lat) * m.cos(lon)
    Y = (N+h) * m.cos(lat) * m.sin(lon)
    Z = (N * (1 - e2_GRS80) + h) * m.sin(lat)
    
    return X,Y,Z

