#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 11:25:08 2024

@author: abettiol
"""

import numpy as np

r2d   = 180/np.pi


def rotmat2euler(R:np.ndarray, outdeg=False):
    
    """convention : u, v, w (yaw, pitch, roll)"""
    
    u = np.arctan(R[2,1]/R[2,2])
    
    v = -np.arcsin(R[2,0])
    
    w = -np.arctan(R[1,0]/R[0,0])
  
    if outdeg:
        
        return np.array([u, v, w]) * r2d
    
    return np.array([u, v, w])

def rotmat2quat(R):
    
    mag_q0 = np.sqrt((1 + R[0,0] + R[1,1] + R[2,2])/4)
    mag_q1 = np.sqrt((1 + R[0,0] - R[1,1] - R[2,2])/4)    
    mag_q2 = np.sqrt((1 - R[0,0] + R[1,1] - R[2,2])/4)
    mag_q3 = np.sqrt((1 - R[0,0] - R[1,1] + R[2,2])/4)
    
    mag_max = max([mag_q0, mag_q1, mag_q2, mag_q3])
    
    if mag_max == mag_q0:

        return mag_q0, (R[2,1]-R[1,2])/(4*mag_q0), (R[0,2]-R[2,0])/(4*mag_q0), (R[1,0]-R[0,1])/(4*mag_q0)

    elif mag_max == mag_q1:
        
        return (R[2,1]-R[1,2])/(4*mag_q1), mag_q1, (R[0,1]+R[1,0])/(4*mag_q1), (R[0,2]+R[2,0])/(4*mag_q1)
    
    elif mag_max == mag_q2:
        
        return (R[0,2]-R[2,0])/(4*mag_q2), (R[0,1]+R[1,0])/(4*mag_q2), mag_q2, (R[1,2]+R[2,1])/(4*mag_q2)
    
    elif mag_max == mag_q3:
        
        return (R[1,0]-R[0,1])/(4*mag_q3), (R[0,2]+R[2,0])/(4*mag_q3), (R[1,2]+R[2,1])/(4*mag_q3), mag_q3


def euler2rotmat(u, v, w, indeg=False):
    
    if indeg:
        
        u, v, w = np.array([u, v, w])[:] * 1/r2d
    
    Rx = np.array([[1,         0,          0],
                   [0, np.cos(u), -np.sin(u)],
                   [0, np.sin(u),  np.cos(u)]])

    Ry = np.array([[ np.cos(v), 0, np.sin(v)],
                   [         0, 1,         0],
                   [-np.sin(v), 0, np.cos(v)]])
    
    Rz = np.array([[np.cos(w), -np.sin(w), 0],
                   [np.sin(w),  np.cos(w), 0],
                   [        0,          0, 1]])
    
    return Rz @ Ry @ Rx

def euler2quat(u, v, w, indeg=False):
    
    if indeg :
        
        u, v, w = np.array([u, v, w])[:] * 1/r2d
        
    q0 = np.cos(u/2)*np.cos(v/2)*np.cos(w/2) + np.sin(u/2)*np.sin(v/2)*np.sin(w/2)
    
    q1 = np.sin(u/2)*np.cos(v/2)*np.cos(w/2) - np.cos(u/2)*np.sin(v/2)*np.sin(w/2)
    
    q2 = np.cos(u/2)*np.sin(v/2)*np.cos(w/2) + np.sin(u/2)*np.cos(v/2)*np.sin(w/2)
    
    q3 = np.cos(u/2)*np.cos(v/2)*np.sin(w/2) - np.sin(u/2)*np.sin(v/2)*np.cos(w/2)
    
    return q0, q1, q2, q3

def quatnorm2quat(x, y, z):
    
    return  np.sqrt(1 - (x**2 + y**2 + z**2))

def quat2rotmat(q0, q1, q2, q3):
    
    
    return np.array([[q0**2 + q1**2 - q2**2 - q3**2, 2*q1*q2 - 2*q0*q3,                          2*q1*q3 + 2*q0*q2],
                     [2*q1*q2 + 2*q0*q3,             q0**2 - q1**2 + q2**2 - q3**2,              2*q2*q3 - 2*q0*q1],
                     [2*q1*q3 - 2*q0*q2,             2*q2*q3 + 2*q0*q1,             q0**2 - q1**2 - q2**2 + q3**2]])


def quat2euler(q0, q1, q2, q3, outdeg=False):
    
    """convention : u, v, w output en degres"""
    
    rotmat = quat2rotmat(q0, q1, q2, q3)
    
    u, v, w = rotmat2euler(rotmat, outdeg)
    
    return u, v, w
