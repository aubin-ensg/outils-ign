#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 14 11:05:21 2024

@author: abettiol
"""

import pandas as pd
from matplotlib.axes import Axes
import re
import numpy as np

def serie_stats(serie:pd.Series):
    
    return {"mean" : serie.mean(),
            "max_idx" : serie.index[serie.argmax()],
            "max" : serie.max(),
            "min_idx" : serie.index[serie.argmin()],
            "min" : serie.min(),
            "std" : serie.std(),
            "abs_mean" : serie.abs().mean(),
            "abs_max_idx" :serie.index[serie.abs().argmax()],
            "abs_max" : serie.abs().max(),
            "abs_min_idx" : serie.index[serie.abs().argmin()],
            "abs_min" : serie.abs().min(),
            "count" : serie.size,
            "abs_std" : serie.abs().std(),
            "abs_quart1" : serie.abs().quantile(0.25),
            "abs_quart2" : serie.abs().quantile(0.5),
            "abs_quart3" : serie.abs().quantile(0.75)
    }

def plot_stats(axs:Axes, serie:pd.Series, abs_stats=False, unit="m", ylim=[], xlabel="", ylabel="", title="", prompt=False, only=False,
               fmt='.3f', fsize=10, loc="best", avrg_pref="abs ", multipat=[], shift_label=4, use_index=False, col_avrg="grey", min_max_label=True):
        
    stats = serie_stats(serie)
    
    if multipat:
        
        for pat in multipat:
            
            filter_index = [N for N in serie.index if re.match(pat, N)]
            serie_filter = serie.loc[filter_index]
            
            if stats["max"][0] in serie_filter.index:
                
                x_max = serie_filter.index.get_loc(stats["max"][0])
                
            if stats["min"][0] in serie_filter.index:
                
                x_min = serie_filter.index.get_loc(stats["min"][0])
                
        x = np.arange(len(serie_filter.index))
        
    else:
        
        x_max = serie.index.get_loc(stats["max"][0])
        x_min = serie.index.get_loc(stats["min"][0])
            
        x = np.arange(len(serie.index))

    if only == "avrg_min_max":
        
        avrg, = axs.plot(x, np.ones(x.shape) * stats["abs_mean"], c=col_avrg, linestyle="dotted")
        axs.legend([avrg], [f"{avrg_pref}avrg = {stats['abs_mean']:{fmt}} " + unit])
        
        bmax = axs.scatter(x_max, stats["max"][1], color="black", s=160, facecolors='none')
        lmax = axs.scatter(x_max, stats["max"][1], color="black", s=80, facecolors='none')
        lmin = axs.scatter(x_min, stats["min"][1], color="black", s=80, facecolors='none')
    
        legend = axs.legend([avrg, (bmax, lmax), (lmin)],
                    [   r"\textbf{" + f"{avrg_pref}avrg = {stats['abs_mean']:{fmt}} " + unit + " }",
                        f"max = {stats['max'][1]:{fmt}} " + unit, 
                        f"min = {stats['min'][1]:{fmt}} " + unit,
                     ],
                    
                   fontsize=fsize, loc=loc)
        

    else:
        
        avrg     = np.ones(x.shape) * stats["abs_mean"]
        std_up   = np.ones(x.shape) * stats["abs_mean"] + stats["std"]
        std_down = np.ones(x.shape) * stats["abs_mean"] - stats["std"]

        avrg,     = axs.plot(x, avrg, c=col_avrg)
        std_up,   = axs.plot(x, std_up, c="black")
        std_down, = axs.plot(x, std_down, c="black")
             
        bmax = axs.scatter(x_max, stats["max"][1], color="black", s=160, facecolors='none')
        lmax = axs.scatter(x_max, stats["max"][1], color="black", s=80, facecolors='none')
        lmin = axs.scatter(x_min, stats["min"][1], color="black", s=80, facecolors='none')
    
        legend = axs.legend([avrg, std_up, (bmax, lmax), (lmin)],
                    [r"\textbf{" + f"{avrg_pref}avrg = {stats['abs_mean']:{fmt}}" + unit + "} " ,
                     f"std = {stats['std']:{fmt}} " + unit,
                     f"max = {stats['max'][1]:{fmt}} " + unit, 
                     f"min = {stats['min'][1]:{fmt}} " + unit],
                   fontsize=fsize, loc=loc)
        
        
    if min_max_label :
        
        axs.annotate(stats["max"][0], [x_max + shift_label, stats["max"][1]])
        axs.annotate(stats["min"][0], [x_min + shift_label, stats["min"][1]])
        
    if ylim:
        axs.set_ylim(ylim)
        
    if xlabel:
        axs.set_xlabel(xlabel)
        
    if ylabel:
        axs.set_ylabel(ylabel + f" [{unit}]")
    
    if title:
        axs.set_title(title)
    
    return axs
