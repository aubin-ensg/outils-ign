# outils-ign

## Prérequis pour l'utilisation :

--> installer virtualenv             : [aide installation virtualenv](https://python-guide-pt-br.readthedocs.io/fr/latest/dev/virtualenvs.html)  
--> créer un environnement virtuel   : `virtualenv venv-tools`  
--> activer l'environnement          : `source venv-tools/bin/activate`  
--> cloner le repository git         : `git clone https://gitlab.com/aubin-ensg/outils-ign.git`  
--> installer les packages requis    : `pip install -r requests.txt` 
--> commencer à utiliser les scripts : `cd outils-ign` 

## geostix_extevent2imgllh.py

--> association positions/images à partir du fichier EXTEVENT.LOG du Geostix et d'un répertoire d'images.

### Utilisation :

--> classique :
    `python3 geostix_extevent2imgllh.py EXTEVENT.LOG ".*JPG"`  
--> voir options :
    `python3 geostix_extevent2imgllh.py -h`  

## geostix_exteventcomp.py

--> comparaison et analyse d'un couple de fichiers EXTEVENT.LOG  

### Utilisation :

--> classique :
    `python3 geostix_exteventcomp.py C1F03G180_EXTEVENT.LOG C2F01G132_EXTEVENT.LOG`  
--> voir options :
    `python3 geostix_exteventcomp.py -h`  

## mm_create_couples.py

--> création des couples d'images d'une acquisition qui présentent potentiellement des points homologues 

### Utilisation :

Exemple :

```python3 /mnt/e/outils-ign/mm_create_couples.py ./ C1_S1.*JPG C2_S1.*JPG C1_S2.*JPG C2_S2.*JPG C1_S3.*JPG C2_S3.*JPG --inter=True -gnss=Ori-GNSS-Poub -oconcat=True -ps=360 -ofmt=micmac```

## mm_eval_predict.py

--> évaluation d'une ou plusieurs prédiction par comparaison avec un jeu de mesures images.

### Utilisation :

--> classique :
    `python3 mm_eval_predict.py TargetsC-Mes2D.xml SimplePredict-GCPsC.xml SimplePredict-GNSS.xml`  
--> voir options :
    `python3 mm_eval_predict.py -h` 
