#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  1 16:46:13 2024

@author: abettiol
"""

from tools import gstx_tools, cam_tools
from pathlib import Path
import pandas as pd
import argparse
import sys


FAIL = "--> Echec de la procédure."
SUCESS = "\t\t--> Succès de la procédure."
OUTPUT_FIELDS = {'path', 'xif_wsec', 'wsec', 'lat', 'code1', 'q1', 'status',
                 'q2', 'wk','lon', 'dV', 'q3', 'dHz', 'h', 'code2', 'img',
                 'E_l93', 'N_l93', 'X', 'Y', 'Z', 'U', 'V', 'W'}
STATUS = ['R', 'F', 'N', '']

def fail():
    
    print(FAIL)
    sys.exit()
    return None


def extevent_2_imgllh(log_file:Path,
                      dir_:Path,
                      pattern:str,
                      status:list=['R'],
                      only_assoc=True,
                      convert2_enh:bool=False,
                      convert2_xyz:bool=False,
                      convert2_uvw:bool=False,
                      timeshift_treshold:int=5):
    
    print("! -- Cette méthode suppose que le premier événement du fichier"\
          + " de log correspond à la première image du répertoire.")
    
    # - Read file/image directory & convert coordinates
    
    # - first reading for logs
    dat  = gstx_tools.read_exteventlog(log_file, status=status)
    # - second for usage
    dat  = gstx_tools.read_exteventlog(log_file,
                                   convert2_enh=convert2_enh,
                                   convert2_uvw=convert2_uvw,
                                   convert2_xyz=convert2_xyz,
                                   log=False)
    
    
    imgs = cam_tools.read_imgdir(dir_, pattern, convert2_wsec=True)
    
    # -- Association sur l'index
            
    if len(dat) == len(imgs):
        
        print(f">> -- Association sur l'index {log_file.name} &"\
              + f" {dir_}/{pattern} --")      
        
        # - Convert path to image name & join GNSS/images on index
        dat.insert(0,"img", imgs["path"].apply(lambda x : x.name))
        
        print(SUCESS)
        
        # - Filter by status and return
        return dat.loc[dat["status"].isin(status)]
    
    # -- Association sur la datation

    else:
        
        print(f">> -- Association sur la datation {log_file.name}/{dir_.name}"\
              + " --")
                    
        timeshift = dat.iloc[0]["wsec"] - imgs.iloc[0]["xif_wsec"]
        imgs["wsec"] = imgs["xif_wsec"] + timeshift
        
        print("\t! -- Le premier événement est décalé de la première "\
              + f"image de {timeshift:.2f} s.")
        
        # - Pivot/base could be either images or GNSS
        base = max([imgs, dat], key=len)
        pivot = min([imgs, dat], key=len)

        # - Add base columns from pivot columns
        for col in set(pivot.columns) - set(base.columns):

                base[col] = pd.NA
        
        mean_ts = 0
        
        for index, row in pivot.iterrows():
            
            # - Looking for the closest GNSS/image couple
            idx = (base["wsec"] - row["wsec"]).abs().argmin(skipna=True)
            
            ts = base.loc[idx]['wsec'] - row['wsec']

            mean_ts += abs(ts)
                        
            # - Reject procedure if timeshift > treshold
            if abs(ts) > timeshift_treshold:
            
                print(f"\t> - {index} GNSS/xif time shift : {ts:.2f} s.")
                
                print("\t!! -- Le décalage entre l'image et la position"\
                      + " associée est supérieure au seuil de"\
                      + f" {timeshift_treshold} s : association rejetée.")
                    
                continue
            
            # - Fill base columns with pivot columns
            base.loc[idx, pivot.columns] = row
        
        print(f"\t> -- mean GNSS/xif time shift : {mean_ts/len(pivot):.2f} s.")
        
        # - Convert path to name
        base['img'] = imgs["path"].apply(lambda x : x.name)
        
        # - Filter by status
        base = base.loc[base["status"].isin(status)]
        
        print()
        
        print(SUCESS)
    
    
        if only_assoc:
            
            # - Reject unmatch GNSS/images
            return base.dropna()
        
        else:
            
            return base
    


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="*************** extevent2imgllh ***************")
    
    parser.add_argument("log_files", nargs='+', help="extevent log files")
    
    parser.add_argument("--dir", type=str, help="images directory"\
                        + " (Default='')", default='')
    
    parser.add_argument("-pat", "--patterns", nargs='+',
                        help="list of image patterns"\
                        +" (one per log file)",
                        default=[])
        
    parser.add_argument("-rtk", "--rtk_status", nargs='+',
                        help="status of GNSS obs."\
                        + f" in {STATUS} (Default=['R'])",
                        default=['R'])
    
    parser.add_argument("--out_infos", nargs='+',
                        help="fields to export in output"\
                        + f" values : {OUTPUT_FIELDS}"\
                        + " (Default=['img', 'lat', 'lon', 'h'])",
                        default=['img', 'lon', 'lat', 'h'])
        
    parser.add_argument("--out", help="output file name"\
                        +" (Default='img_llh.txt')",
                        default='img_llh.txt')
        
    parser.add_argument("--only_assoc", help="no export for unmatch"\
                        +" GNSS/image couples (Default=False)",
                        default='True')
        
    parser.add_argument("-t", "--timeshift_treshold", type=int,
                        help="treshold for timeshift"\
                        +" between GNSS and corrected image datation"\
                        +" (Default=5)",
                        default=5)  
    
    args = parser.parse_args()

    
    print("\n*************** extevent2imgllh ***************")
    print("LOG FILE(S)   :", args.log_files)
    print("IMAGE DIR.    :", args.dir)
    print("PATTERN(S)    :", args.patterns)
    print("RTK STATUS    :", args.rtk_status)
    print("OUTPUT        :", args.out)
    print("OUTPUT FIELDS :", args.out_infos)
    print("ONLY ASSOC.   :", args.only_assoc)
    print("TIMESHIFT TR. :", args.timeshift_treshold)
    print("\n")
    
    evaluate = {
        "True":True,
        "true":True,
        "False":False,
        "false":False
        }
    
    if len(set(STATUS).intersection(set(args.rtk_status))) \
        != len(args.rtk_status):
        
        print("!! -- Les états"\
              + f" {set(args.rtk_status).difference(set(STATUS))}"\
              + " ne sont pas pris en charge.") 
    
    
    if not args.dir:
        
        for index, log in enumerate(args.log_files):
            
            dat  = gstx_tools.read_exteventlog(Path(log), status=args.rtk_status)
            
            sys.exit()
            
            fail()
            
    else:
        
        if not Path(args.dir).is_dir():
            
            print(f"!! -- {args.dir} n'est pas un dossier.")
            
            fail()
            
        if len(args.log_files) != len(args.patterns):
            
            print("!! -- Il n'y a pas autant de fichiers d'événements"\
                  + " que de pattern d'images.")

            fail()
        
        if len(OUTPUT_FIELDS.intersection(set(args.out_infos))) \
            != len(args.out_infos):
            
            print("!! -- Les infos"\
                  + f" {set(args.out_infos).difference(OUTPUT_FIELDS)}"\
                  + " ne peuvent pas être exportées.")
                
            fail()
            
        concat = []
        convert2_enh = 'E_l93' in args.out_infos or 'N_l93' in args.out_infos
        convert2_xyz = 'X' in args.out_infos or 'Y' in args.out_infos\
            or 'Z' in args.out_infos
        convert2_uvw = 'U' in args.out_infos or 'V' in args.out_infos\
            or 'W' in args.out_infos

    
        for index, log in enumerate(args.log_files):
            
            pat = args.patterns[index]
            
            base = extevent_2_imgllh(
                log_file=Path(log),
                dir_=Path(args.dir),
                status=args.rtk_status,
                pattern=pat,
                convert2_enh=convert2_enh,
                convert2_xyz=convert2_xyz,
                convert2_uvw=convert2_uvw,
                only_assoc=evaluate[args.only_assoc],
                timeshift_treshold=args.timeshift_treshold)
            
            concat.append(base)
            
        concat = pd.concat(concat)
        concat.to_csv(Path(args.dir).joinpath(args.out), sep=' ',
                      columns=args.out_infos, header=False,
                      index=False)
                
    print("--> Succès de extevent2imgllh : OUTPUT ="\
          + f" {Path(args.dir).joinpath(args.out)}"\
          + f" : {len(concat.dropna())} associations.")
    
    print("***********************************************\n")
